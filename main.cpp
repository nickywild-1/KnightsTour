#include <iostream>
#include <stdio.h>
#include <windows.h>

using namespace std;

int N = 8;
int startCount;
int& moveCount = startCount;
int* tour = new int[N*N];

int startX;
int startY;

//Function called before calculating the Tour, allows the user to set a valid board size and starting position
void customSize() {
	cout << "Please enter your boardsize N (5-36)" << endl;
	cin >> N;
	if(N < 5 || N > 35) {
		cout << "Invalid size" << endl;
		exit (EXIT_FAILURE);
	}
	
	cout << "Please enter your startposition X (0-" << N-1 << ")" << endl;
	cin >> startX;
	if(startX >= N) {
		cout << "Invalid position" << endl;
		exit (EXIT_FAILURE);
	}
	
	cout << "Please enter your startposition Y (0-" << N-1 << ")" << endl;
	cin >> startY;
	if(startY >= N) {
		cout << "Invalid position" << endl;
		exit (EXIT_FAILURE);
	}
	
	startCount = 1;
}

//Move structure
typedef struct moves {
	int x;
	int y;
}
moves;

bool movePossible(moves moveTo) {
	int i = moveTo.x;
	int j = moveTo.y;
		
	if ((i >= 0 && i < N) && (j >= 0 && j < N) && (tour[i*N+j] == 0)) {
		return true;
	}
	return false;
}

//Recursively looks for a Tour -- Modify to use a heuristic
bool calculateTour(moves validMoves[], moves moveFrom) {
	int i;
	int score;
	int lowestScore = 8;
	moves moveTo = {0,0};
	moves moveToTwo = {0,0};
	moves lowestMove = {0,0};
	
	tour[moveFrom.x*N+moveFrom.y] = moveCount;
		
	if (moveCount == N*N) {
		return true;
	}
	
	//Tries possible moves from current position
	for (i = 0; i < 8; i++) {
		score = 0;
		moveTo.x = moveFrom.x + validMoves[i].x;
		moveTo.y = moveFrom.y + validMoves[i].y;
		
		if(movePossible(moveTo)) {
			for(int j = 0; j < 8; j++){
				moveToTwo.x = moveTo.x + validMoves[j].x;
				moveToTwo.y = moveTo.y + validMoves[j].y;
				
				if(movePossible(moveToTwo)) {
					score++;
				}
			}
			
			if(score == 0) {
				lowestScore = score;
				lowestMove.x = moveTo.x;
				lowestMove.y = moveTo.y;
			}
			else if(score < lowestScore) {
				lowestScore = score;
				lowestMove.x = moveTo.x;
				lowestMove.y = moveTo.y;
			}
			else if(score == lowestScore) {
				if(moveTo.x == 0 || moveTo.x == N-1) {
					lowestScore = score;
					lowestMove.x = moveTo.x;
					lowestMove.y = moveTo.y;
				}
				else if(moveTo.y == 0 || moveTo.y == N-1) {
					lowestScore = score;
					lowestMove.x = moveTo.x;
					lowestMove.y = moveTo.y;
				}
			}
		}
	}
	
	moveCount++;
		if (calculateTour(validMoves, lowestMove) == true) {
			return true;
		}
		else {
			return false;	
		}
}

//Visualizes the Tour
void knightsTour() {
	int i;
	int j;
	
	//Tour Matrix
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			tour[i*N+j] = 0;
		}
	}
	
	//Moves allowed for the Knight, prioritized according to Warnsdorf
	moves validMoves[8] = { {2,1}, {1,2}, {-1,2}, {-2,1}, {-2,-1}, {-1,-2}, {1,-2}, {2,-1} };
	
	//Checks whether the Tour is possible using the calculateTour function
	moves moveFrom = {startX, startY};
	if(calculateTour(validMoves, moveFrom) == false) {
		cout<<"\nTour cannot be found";
	}
	else {
		int k;
		int l;
		for (k = 0; k < N; k++) {
			for (l = 0; l < N; l++) {
				cout << tour[k*N+l] << "\t";
			}
			cout << endl;
		}
		cout << "\nTour found";
	}
}

//Main
int main() {
	customSize();
	
	//Making sure bigger Tours can be displayed as well -- Crude but somewhat effective
	SMALL_RECT rect;
	COORD coord;
	coord.X = N*10; //Buffer size
	coord.Y = N*10;
	
	rect.Top = 0;
	rect.Left = 0;
	rect.Bottom = coord.Y-1; //Height
	rect.Right = coord.X-1;  //Width
	
	HANDLE hwnd = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleScreenBufferSize(hwnd, coord);
	SetConsoleWindowInfo(hwnd, TRUE, &rect);
	
	//Tour
	knightsTour();
	cout<<endl;
	system ("pause");
	return 0;
}
